# Simple switcher card

Simple ARM based card that should measure voltage and turn on/off a FET depending on voltage.

Can also be used as a simple diagnostic tool for various stuff.

  * Shall have a FET auto on/off.
    * When 12v system: Turn on FET between > 12 Vdc <= 14.2 Vdc
    * When 24v system: Turn on FET between > 24 Vdc <= 28.4 Vdc
  * Button to turn on auto onoff function.
  * FET shall support up to 5A 12V (~60Watt) or 24v ~ 2.5A. 
  * Shall draw max 0.1mA when sleep.
  * Display shall only be active when button is pressed.
  * USB CDC shall print voltage every X second when USB is connected to a Unix computer.

# Components

  * STM32F042F6.
  * USB micro connector (module).
  * Terminal block for input.
  * Terminal block for load.
  * Circuit protect PFET SI2319CDS
  * FET for OLED (TBD 100mA 3.3v)
  * FET for LOAD
    * Infeneon PFET 650P
  * Switch regulator that support input 30v, output 3.3v.
    * MCP16311 	
  * Optional LDO 5v to 3.3v for USB VCC Input.
    * MCP1702T
  * OLED Display DSD tech (or similar)
  * 4 tactile buttons
  * Capacitors see table below.
  * Resistors see table below.

## Capacitors

| Value  | Quantity | Description         |
| -------| :-------:| :-------------------|
| 100nF  | 6        | STM32 (Buttons/VCC) |
| 10nF   | 1        | STM ADC_INPUT       |
| 1uF    | 2        | MCP1702T IN/OUT     |
| 10uF   | 4        | MCP16311 IN/OUT     |

## Resistors

| Value  | Quantity | Description                 |
| -------| :-------:| :---------------------------|
| 10k    | 5        | Switch, 2 * FET's, BTN, RST |
| 4.7k   | 2        | I2C                         |
| 31.6k  | 1        | Switch                      |
| 47K    | 1        | ADC_INPUT                   |
| 13.3K  | 1        | ADC_INPUT                   |
| TBD    | 2        | 2 * FET gate                |

### ADC Voltage divider 

 * 47K
 * 13.3K

## Inductor

* 15uH for MCP16311

# Pin configuration

| Pin  | Name          | Description         |
| -----| :------------:| -------------------:|
| PB8  | BOOT0         | pull down           |
| PF0  | I2C_DATA      | OLED I2C Data       |
| PF1  | I2C_CLK       | OLED I2C Clock      |
| NRST | BUTTON RST    | Reset via cap       |
| VDDA | 3_3V          | 3.3v (100nF cap)    |
| PA0  | ADC_INPUT     | 12v measure         |
| PA1  | INPUT         |                     |
| PA2  | SWITCH_OLED   | connect to OLED FET |
| PA3  | SWITCH_LOAD   | connect to FET      |
| PA4  | N.C.          | Maybe pulldown      |
| PA5  | BUTTON_AUTO   | Auto on/off         |
| PA6  | BUTTON_OLED   | OLED on/off         |
| PA7  | BUTTON_X      |                     |
| PB1  | N.C.          | Maybe pulldown      |
| VSS  | GND           | GND                 |
| VCC  | 3_3V          | 3.3v (100nF cap)    |
| PA11 | USB_DM        | USB-                |
| PA12 | USB_DP        | USB+                |
| PA13 | SWD_IO        |                     |
| PA14 | SWD_CLK       |                     |

*Note! USB pins are multiplexed with PA9/10 and need to be programmed via fuses.*

